package app;

import graph.GNode;
import graph.Graph;
import graph.GraphHandler;

import java.util.Arrays;
import java.util.List;

public class Launcher {
    public static void main(String[] args) {
        Graph J = Graph.createGraph("J", null);
        Graph I = Graph.createGraph("I", null);
        Graph H = Graph.createGraph("H", null);
        Graph G = Graph.createGraph("G", null);
        Graph F = Graph.createGraph("F", null);
        Graph E = Graph.createGraph("E", Arrays.asList(J).toArray(new GNode[0]));
        Graph D = Graph.createGraph("D", null);
        Graph C = Graph.createGraph("C", Arrays.asList(G, H, I).toArray(new GNode[0]));
        Graph B = Graph.createGraph("B", Arrays.asList(E, F).toArray(new GNode[0]));
        Graph A = Graph.createGraph("A", Arrays.asList(B, C, D).toArray(new GNode[0]));


        List<GNode> nodes = GraphHandler.walkGraph(A);
        nodes.forEach(node -> System.out.print(node.getName() + " "));

        System.out.println();

        List<List<GNode>> paths = GraphHandler.paths(A);

        paths.forEach(path -> System.out.print(path + " "));
    }
}
