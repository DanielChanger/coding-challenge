package graph;

import java.util.Arrays;
import java.util.Objects;

/**
 * <h1>Graph</h1>
 *
 * <p>Class, which represents graph itself.</p>
 *
 * @author Danylo Miniailo.
 * @version 1.0.
 * @since 2018-10-12.
 */
public class Graph implements GNode {

    /**
     * Name of root node of graph.
     */
    private String name;

    /**
     * Sub-nodes of root node.
     */
    private GNode[] children;

    /**
     * Constructor for instantiating Graph object.
     * @param name     Name of root node of graph.
     * @param children Sub-nodes of root node.
     */
    private Graph(String name, GNode[] children) {
        this.name = name;
        this.children = children;
    }

    public static Graph createGraph(String name, GNode[] children) {
        if (name == null) {
            throw new NullPointerException("Name cannot be null");
        } else if (name.equals("")) {
            throw new IllegalArgumentException("You should provide any name for graph root node");
        } else {
            return new Graph(name, children);
        }
    }

    /**
     * Method to obtain root node name.
     * @return Name of root node of graph.
     */
    @Override
    public String getName() {
        return this.name;
    }

    /**
     * Method to obtain root node children.
     * @return Sub-nodes of root node. If children field is null or has zero-length,
     * then it returns zero-length array of GNode.
     */
    @Override
    public GNode[] getChildren() {
        if (children == null || children.length == 0) {
            return new GNode[0];
        }
        return children;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }

        Graph graph = (Graph) obj;
        return Objects.equals(this.name, graph.getName()) && Arrays.deepEquals(this.children, graph.getChildren());
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + name.hashCode();
        result = 31 * result + Arrays.deepHashCode(getChildren());
        return result;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
