package graph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <h1>Graph Handler</h1>
 *
 * <p>This class allows to obtain graph nodes and all of the paths from the exact node. </p>
 *
 * @author Danylo Miniailo.
 * @version 1.0.
 * @since 2018-10-12.
 */
public class GraphHandler {

    /**
     * Private constructor of GraphHandler class to not allow anyone to create an instance.
     */
    private GraphHandler() {
    }

    /**
     * Recursively obtains all of the sub-nodes including sub-nodes of sub-nodes etc.,
     * and returns list of all nodes of the graph and the root node.
     * @param node GNode object, which represents a graph or a single node of graph.
     * @return list of all nodes of graph.
     */
    public static List<GNode> walkGraph(GNode node) {
        List<GNode> children = new ArrayList<>();

        if (node.getChildren().length == 0) {
            return new ArrayList<>(Collections.singletonList(node));
        }

        children.add(0, node);
        for (GNode temp : node.getChildren()) {
            children.addAll(walkGraph(temp));
        }

        return children.stream()
                .distinct()
                .collect(Collectors.toList());
    }

    /**
     * Method, which returns list of every possible path from the passed node.
     * @param node GNode object, which represents a graph or a single node of graph.
     * @return list of lists of all of the paths starting from the passed node.
     */
    public static List<List<GNode>> paths(GNode node) {
        List<List<GNode>> paths = new ArrayList<>();
        List<GNode> currentPath = new ArrayList<>();
        addPath(node, currentPath, paths);
        return paths;
    }

    /**
     * Supporting method, which helps recursively obtain all paths of the node.
     * @param node GNode object, which represents a graph or a single node of graph.
     * @param currentPath list of nodes, which defines one of the path.
     * @param paths list of lists of all of the paths starting from the passed node.
     */
    private static void addPath(GNode node, List<GNode> currentPath, List<List<GNode>> paths) {

        currentPath.add(node);

        GNode[] children = node.getChildren();
        if (children.length != 0) {
            for (GNode child : children) {
                addPath(child, currentPath, paths);
            }
        } else {
            paths.add(new ArrayList<>(currentPath));
        }

        currentPath.remove(currentPath.size() - 1);
    }
}
