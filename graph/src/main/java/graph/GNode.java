package graph;

public interface GNode {
    String getName();
    GNode[] getChildren();
}
