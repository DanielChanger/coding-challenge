package graph;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

/**
 * Graph test class.
 */
class GraphTest {

    /**
     * Tests if createGraph method throws NullPointerException if null is passed as the name argument.
     */
    @Test
    void testCreateGraphThrowsNullPointerExceptionIfNameIsNull() {
        Assertions.assertThrows(NullPointerException.class, () -> Graph.createGraph(null, null));
    }

    /**
     * Tests if createGraph method throws IllegalArgumentException if blank string is passed as the name argument.
     */
    @Test
    void testCreateGraphThrowsIllegalArgumentExceptionIfNameIsBlank() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> Graph.createGraph("", null));
    }

    /**
     * Tests if createGraph method doesn't throw any exceptions and works fine.
     */
    @Test
    void testCreateGraphDoesntThrowAnyExceptionAndCreatesObjectIfNameIsNotBLackAndNotNull() {
        Graph.createGraph("A", null);
    }

    /**
     * Tests if getChildren method returns zero-length GNode array, if children field is null.
     */
    @Test
    void testGetChildrenReturnsZeroLengthArrayIfChildrenFieldIsNull() {
        //given
        GNode[] expected = new GNode[0];

        //when
        Graph graph = Graph.createGraph("A", null);
        GNode[] actual = graph.getChildren();

        //then
        Assertions.assertArrayEquals(expected, actual);
    }

    /**
     * Tests equals method returns true, if fields of two graphs are equal.
     */
    @Test
    void testGraphA1andA2Equal() {
        Graph F = Graph.createGraph("F", null);
        Graph E = Graph.createGraph("E", Arrays.asList(F).toArray(new GNode[0]));
        Graph D = Graph.createGraph("D", null);
        Graph C = Graph.createGraph("C", Arrays.asList(E, D).toArray(new GNode[0]));
        Graph A1 = Graph.createGraph("A", Arrays.asList(C, D).toArray(new GNode[0]));
        Graph A2 = Graph.createGraph("A", Arrays.asList(C, D).toArray(new GNode[0]));

        assertEquals(A1, A2);
    }

    /**
     * Tests equals method returns false, if fields of two graphs are not equal.
     */
    @Test
    void testGraphA1andA2NotEqual() {
        Graph F = Graph.createGraph("F", null);
        Graph E = Graph.createGraph("E", (GNode[]) Arrays.asList(F).toArray());
        Graph D = Graph.createGraph("D", null);
        Graph C = Graph.createGraph("C", (GNode[]) Arrays.asList(E, D).toArray());
        Graph A1 = Graph.createGraph("A", (GNode[]) Arrays.asList(C, F).toArray());
        Graph A2 = Graph.createGraph("A", (GNode[]) Arrays.asList(C, D).toArray());

        assertNotEquals(A1, A2);
    }

    /**
     * Tests hashCode method returns different value, if two graphs has the same name, but different children.
     */
    @Test
    void testGraphHashCodeNotEqualsIfGraphAreDifferent() {
        GNode[] children = new GNode[2];
        GNode[] childrenOfC = new GNode[1];
        children[0] = Graph.createGraph("B", null);
        children[1] = Graph.createGraph("C", childrenOfC);
        childrenOfC[0] = Graph.createGraph("E", null);

        Graph A1 = Graph.createGraph("A", children);
        Graph A2 = Graph.createGraph("A", (GNode[]) Arrays.asList(childrenOfC[0], children[0]).toArray());

        int expected = A1.hashCode();
        int actual = A2.hashCode();

        Assertions.assertNotEquals(expected, actual);
    }
}