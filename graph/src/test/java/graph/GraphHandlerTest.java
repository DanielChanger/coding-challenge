package graph;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * GraphHandler test class.
 */
class GraphHandlerTest {

    private Graph A, B, C, D, E, F, G, H, I, J; //graphs
    private GraphHandler graphHandler;

    /**
     * Setting up data for testing by instantiating each graph.
     */
    @BeforeEach
    void setUp() {
        J = Graph.createGraph("J", null);
        I = Graph.createGraph("I", null);
        H = Graph.createGraph("H", null);
        G = Graph.createGraph("G", null);
        F = Graph.createGraph("F", null);
        E = Graph.createGraph("E", Arrays.asList(J).toArray(new GNode[0]));
        D = Graph.createGraph("D", null);
        C = Graph.createGraph("C", Arrays.asList(G, H, I).toArray(new GNode[0]));
        B = Graph.createGraph("B", Arrays.asList(E, F).toArray(new GNode[0]));
        A = Graph.createGraph("A", Arrays.asList(B, C, D).toArray(new GNode[0]));
    }

    /**
     * Checks if "walkGraphs" method executes as expected and returns predictable value.
     */
    @Test
    void testWalkGraph() {
        //given
        List<GNode> expected = new ArrayList<>(Arrays.asList(B, E, J, F));

        //when
        List actual = GraphHandler.walkGraph(B);

        //then
        assertEquals(expected, actual);
    }

    /**
     * Checks if "paths" method executes as expected and returns predictable value.
     */
    @Test
    void testPaths() {
        //given
        List<List<GNode>> expected = new ArrayList<>();
        expected.add(Arrays.asList(B, E, J));
        expected.add(Arrays.asList(B, F));

        //when
        List<List<GNode>> actual = GraphHandler.paths(B);

        //then
        Assertions.assertEquals(expected, actual);
    }
}