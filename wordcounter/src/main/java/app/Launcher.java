package app;

import wordcounter.WordCounter;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Map;

public class Launcher {
    public static void main(String[] args) {
        String path;
        if (args.length == 1) {
            path = args[0];
        } else {
            path = "wordcounter" + File.separator + "src" + File.separator + "main" + File.separator
                + "resources" + File.separator + "textFile.txt";
        }
        try {
            WordCounter wordCounter = WordCounter.createWordCounter(
                Paths.get(path));
            Map<String, Integer> wordCounterTable = wordCounter.getWordCountMap();
            for (Map.Entry<String, Integer> entry : wordCounterTable.entrySet()) {
                System.out.println(entry.getKey() + " : " + entry.getValue());
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

}
