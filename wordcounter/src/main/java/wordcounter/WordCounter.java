package wordcounter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.util.Map;
import java.util.TreeMap;

/**
 * <h1>Word Counter.</h1>
 *
 * <p>This app simply counts appearances of each word in a text file.</p>
 *
 * @author Danylo Miniailo.
 * @version 1.0.
 * @since 2018-10-12.
 */
public class WordCounter {

    /**
     * Path of the text file.
     */
    private final Path path;

    /**
     * Simple constructor for creating WordCounter object. It's private to allow user to instantiate
     * object of WordCounter class only via createWordCounter method, which validates file path.
     *
     * @param path text file path.
     */
    private WordCounter(Path path) {
        this.path = path;
    }

    /**
     * Method for instantiating WordCounter object. Its common mission is to check whether file path is valid or not,
     * and throw an exception on demand.
     * @param path text file path.
     * @return WordCount object.
     * @throws IOException throws the IOException, when file path is not valid or if it's null.
     */
    public static WordCounter createWordCounter(Path path) throws IOException {
        try {
            return new WordCounter(path.toRealPath());
        } catch (IOException | NullPointerException | InvalidPathException x) {
            throw new IOException("File not found.");
        }
    }

    /**
     * Method, which returns ordered map of words as keys and numbers of appearances of each word in the text as values.
     * It simply reads all the lines from the text from the file, converts all text to be lowercase to simplify
     * counting (e.g. He = he, so He is the same word as he, not the different ones). The "word" is defined as everything
     * that fits "\\W+" regex.
     * @return Ordered map of words as keys and the number of appearance of each one.
     * @throws IOException if readAllLines execution failed then IOException is thrown.
     */
    public Map<String, Integer> getWordCountMap() throws IOException {
        final String WORD_REGEX = "\\W+";
        String text = String.join("\n", Files.readAllLines(path)).toLowerCase();
        Map<String, Integer> wordCountTable = new TreeMap<>();

        for (String word : text.split(WORD_REGEX)) {
            wordCountTable.merge(word, 1, (oldVal, increment) -> oldVal + increment);
        }
        return wordCountTable;
    }

}
