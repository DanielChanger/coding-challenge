package wordcounter;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.File;
import java.io.IOException;
import java.nio.file.InvalidPathException;
import java.nio.file.Paths;
import java.util.Map;
import java.util.TreeMap;

import static org.junit.jupiter.api.Assertions.fail;

/**
 * WordCounter test class.
 */
class WordCounterTest {

    /**
     * Data provider of invalid paths.
     */
    private static Object[][] getInvalidPaths() {
        return new Object[][] {
                {"asdgas", IOException.class},
                {" ", InvalidPathException.class},
                {"C:\\Users\\Daniel Changer\\Desktop\\Softserve\\Elementary Tasks\\elem_tasks\\envelopes\\src\\main\\resources\\file.txt", IOException.class},
                {"C:\\Users\\Daniel Changer\\Desktop\\Softserve\\Elementary Tasks\\elem_tasks\\fileparser\\src\\test\\testFile.txt1", IOException.class},
                {"1235", IOException.class},
                {"PATH", IOException.class},
                {null, NullPointerException.class},
        };
    }

    /**
     * Checks if "createWordCounter" method throws an exception due to illegal file paths
     */
    @ParameterizedTest
    @MethodSource("getInvalidPaths")
    void testWordCounterCreatorTakesWrongPathsThrowsExceptions(String path, Class expected) {
        Exception exception = null;
        try {
            WordCounter.createWordCounter(Paths.get(path));
        } catch (IOException | NullPointerException | InvalidPathException e) {
            exception = e;
        }
        if (exception == null) {
            fail(expected.toString() + " is expected to be thrown");
        }
    }

    /**
     * Tests if "createWordCounter" method creates WordCount object if legal file path is passed
     */
    @Test
    void testWordCounterCreatorTakesLegalPathAndCreatesObjectWithNoException() throws IOException {
        WordCounter.createWordCounter(Paths.get("src" + File.separator + "test" + File.separator
                + "resources" + File.separator + "textFile.txt"));
    }

    /**
     * Tests if "getWordCountMap" count appearance of each word in a text properly
     */
    @Test
    void testGetWordCountMap() throws IOException {

        //given
        Map<String, Integer> expected = new TreeMap<>();
        expected.put("lorem", 1);
        expected.put("ipsum", 1);
        expected.put("dolor", 2);
        expected.put("sit", 1);
        expected.put("amet", 1);
        expected.put("consectetuer", 1);
        expected.put("adipiscing", 1);
        expected.put("elit", 1);
        expected.put("aenean", 2);
        expected.put("commodo", 1);
        expected.put("ligula", 1);
        expected.put("eget", 1);
        expected.put("massa", 1);

        //when
        WordCounter wordCounter = WordCounter.createWordCounter(Paths.get("src" + File.separator + "test" + File.separator
                + "resources" + File.separator + "textFile.txt"));
        Map<String, Integer> actual = wordCounter.getWordCountMap();

        //then
        Assertions.assertEquals(expected, actual);
    }
}